/**
 * Metro configuration for React Native
 * https://github.com/facebook/react-native
 *
 * @format
 */
const localPackagePaths = [
  '/Users/macbook-m1-2022/Documents/otherProject/blesspayment',
]


module.exports = {
  transformer: {
    getTransformOptions: async () => ({
      transform: {
        experimentalImportSupport: false,
        inlineRequires: true,
      },
    }),
  },
  resolver: {
    nodeModulesPaths: [...localPackagePaths], // update to resolver
  },
  watchFolders: [...localPackagePaths], // update to watch
};
