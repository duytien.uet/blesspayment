import React from "react";
import { Text, useColorScheme, View } from "react-native";
import Icon from "react-native-dynamic-vector-icons";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";
import { isReadyRef, navigationRef } from "react-navigation-helpers";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
/**
 * ? Local & Shared Imports
 */
import { SCREENS } from "@shared-constants";
import HomeScreen from "screens/home/HomeScreen";
import TransferScreen from "screens/transfer/TransferScreen";
import ManageScreen from "screens/manage/ManageScreen";
import DetailScreen from "screens/detail/DetailScreen";
import { DarkTheme, LightTheme, palette } from "shared/theme/themes";
// ? Screens

// ? If you want to use stack or tab or both
const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

const Navigation = () => {
  const scheme = useColorScheme();
  const isDarkMode = scheme === "dark";

  React.useEffect((): any => {
    return () => (isReadyRef.current = false);
  }, []);

  const showLabel = (route: any) => {
    return route.name === SCREENS.TRANSFER ? false : true;
  }

  const initSize = (routeName: string, size: any) => {
    switch(routeName) {
      case SCREENS.TRANSFER:
        return size * 2;
      default:
        return size;
    }
  }

  const renderTabIcon = (
    route: any,
    focused: boolean,
    color: string,
    size: number,
  ) => {
    let iconName = "home";
    switch (route.name) {
      case SCREENS.HOME:
        iconName = focused ? "home" : "home-outline";
        break;
      case SCREENS.SEARCH:
        iconName = focused ? "search" : "search-outline";
        break;
      case SCREENS.NOTIFICATION:
        iconName = focused ? "notifications" : "notifications-outline";
        break;
      case SCREENS.PROFILE:
        iconName = focused ? "person" : "person-outline";
        break;
      case SCREENS.MANAGE:
        iconName = focused ? "account-circle" : "account-circle-outline";
        break
      case SCREENS.TRANSFER:
        iconName = focused ? "bank-transfer" : "bank-transfer";
        break
      default:
        iconName = focused ? "home" : "home-outline";
        break;
    }
    return (
      <View style={{alignItems:"center"}}>
        <Icon name={iconName} type={"MaterialCommunityIcons"} size={initSize(route.name, size)} color={color} />
        { showLabel(route) ? <Text>{route.name}</Text> : null }
      </View>
    );
  };

  const renderTabNavigation = () => {
    return (
      <Tab.Navigator
        screenOptions={({ route }) => ({
          headerShown: false,
          tabBarIcon: ({ focused, color, size }) =>
            renderTabIcon(route, focused, color, size),
          tabBarShowLabel: false,
          tabBarActiveTintColor: palette.primary,
          tabBarInactiveTintColor: "gray",
          tabBarStyle: {
            backgroundColor: isDarkMode ? palette.black : palette.white,
            alignSelf:'center',
            width: '80%',
            margin:26,
            borderRadius:10,
            height: 60,
            padding: 6,
          },
        })}
      >
        <Tab.Screen name={SCREENS.HOME} component={HomeScreen} />
        <Tab.Screen
          name={SCREENS.TRANSFER}
          component={TransferScreen}
        />
        <Tab.Screen name={SCREENS.MANAGE} component={ManageScreen} />
      </Tab.Navigator>
    );
  };

  return (
    <NavigationContainer
      ref={navigationRef}
      onReady={() => {
        isReadyRef.current = true;
      }}
      theme={isDarkMode ? DarkTheme : LightTheme}
    >
      <Stack.Navigator screenOptions={{ headerShown: false }}>
        <Stack.Screen name={SCREENS.HOME} component={renderTabNavigation} />
        <Stack.Screen name={SCREENS.DETAIL}>
          {(props) => <DetailScreen {...props} />}
        </Stack.Screen>
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Navigation;
