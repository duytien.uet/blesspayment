import React, { useEffect, useMemo } from "react";
import { View, FlatList } from "react-native";
import { useTheme } from "@react-navigation/native";
import Icon from "react-native-dynamic-vector-icons";
import { SafeAreaView } from "react-native-safe-area-context";
import * as NavigationService from "react-navigation-helpers";
/**
 * ? Local Imports
 */
import createStyles from "./HomeScreen.style";
import MockData from "./mock/MockData";
import CardItem from "./components/card-item/CardItem";
/**
 * ? Shared Imports
 */
import { SCREENS } from "@shared-constants";
import Text from "@shared-components/text-wrapper/TextWrapper";
import { NetWorkService } from "library/networking";

interface HomeScreenProps {}

const HomeScreen: React.FC<HomeScreenProps> = () => {
  const theme = useTheme();
  const { colors } = theme;
  const styles = useMemo(() => createStyles(theme), [theme]);

  const handleItemPress = () => {
    NavigationService.push(SCREENS.DETAIL);
  };

  /* -------------------------------------------------------------------------- */
  /*                               Render Methods                               */
  /* -------------------------------------------------------------------------- */

  const Header = () => (
    <View style={styles.header}>
      <Welcome />
      <Icon
        name="notifications"
        type="MaterialIcons"
      />
    </View>
  );

  useEffect(() => {
    NetWorkService.Get({url: '/Country/GetAll'}).then((res) => {
      console.log(res.data);
      console.log("ahjhi");
      
    });
  })

  const List = () => (
    <View style={styles.listContainer}>
      <FlatList
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        data={MockData}
        renderItem={({ item }) => (
          <CardItem data={item} onPress={handleItemPress} />
        )}
      />
    </View>
  );

  const Welcome = () => (
    <>
      <Text h3 bold color={colors.text}>
        Hello Kuray
      </Text>
    </>
  );

  const Content = () => (
    <View style={styles.contentContainer}>
      <List />
      <Text h3 bold color={colors.text}>
        Who are you going to send $$ to?
      </Text>
    </View>
  );

  return (
    <SafeAreaView style={styles.container}>
      <Header />
      <Content />
    </SafeAreaView>
  );
};

export default HomeScreen;
