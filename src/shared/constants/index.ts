// ? Screens
export const SCREENS = {
  HOME: "Home",
  SEARCH: "Search",
  NOTIFICATION: "notification",
  PROFILE: "Profile",
  DETAIL: "Detail",
  MANAGE: "Manage",
  TRANSFER: "Transfer"
};
